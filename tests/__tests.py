# coding.utf-8

import sys
import unittest
import requests

sys.path.append("..")
from functionsClass import Crypto


class TestCrypto(unittest.TestCase):

    def test_date_not_none(self):
        self.assertIsNotNone(Crypto.mydatetime, 'Erreur, Champs date Vide')

    def test_request_not_none(self):
        TOKEN = 'EUR'
        for i in Crypto.crypto_list:
            response = requests.get(
                f'{Crypto.BASE_URL}{i}&tsyms={TOKEN}')
        self.assertIsNotNone(response, 'Erreur, la requête n\'a pas aboutie')


if __name__ == '__main__':
    unittest.main()
